#!/bin/bash

if [ $# -lt 1 ]
then
    echo "Usage: $0 <nExpt> [firstExpt = 0]"
    exit 1
fi

nexpt=$1
firstexpt=0
numslaves=2
TagData="Tag10_CP0.dat"
UnTagData="MagPhase.dat"

if [ $# -gt 1 ]
then
    firstexpt=$2
fi

# Do whatever you need to do to setup your ROOT environment

echo " use -k5reauth screen- and -aklog- before running it in a screen"

# Generate the toy MC
if [ ! -e   Gen_Tag10_CP0.root ]
then
    echo "Generating MC for Tagged category"
    ./GenFitCP $TagData gen $nexpt $firstexpt > gen-log-Tagged.out 2>&1
fi
if [ ! -e Gen_MagPhase.root  ]
then
    echo "Generating MC for UnTagged category"
    ./GenFitSF $UnTagData gen $nexpt $firstexpt > gen-log-UnTagged.out 2>&1
fi



# Do the simultaneous fit
for ifit in `seq 0 0`
do
    echo "Running fit $ifit"

    ./Master $ifit $nexpt $firstexpt $numslaves > master-log-$ifit-$firstexpt.out 2>&1 &
    sleep 5

    NUMOFLINES=$(wc -l < "master-log-$ifit-$firstexpt.out")
    while [ $NUMOFLINES -lt 1 ]
    do
        sleep 1
        NUMOFLINES=$(wc -l < "master-log-$ifit-$firstexpt.out")
    done

    port=`tail -1 master-log-$ifit-$firstexpt.out | awk '{print $NF}'`
    echo $port 

    ./GenFitCP $TagData fit $ifit $port localhost $nexpt $firstexpt > slave-tagged-log-$ifit-$firstexpt.out 2>&1 &
    sleep 5
    ./GenFitSF $UnTagData fit $ifit $port localhost $nexpt $firstexpt > slave-untagged-log-$ifit-$firstexpt.out 2>&1
done
: '
# Extract the best fit
echo "Extracting the best fit results"

ls fitT*.root > input-list-Tagged.txt
ls fitU*.root > input-list-UnTagged.txt
ls master-ntuple-*.root > input-list-master.txt

./ResultsExtractorMain $nexpt input-list-Tagged.txt best-fits-Tagged.root > resultsextractor-Tagged.out 2>&1
./ResultsExtractorMain $nexpt input-list-UnTagged.txt best-fits-UnTagged.root > resultsextractor-UnTagged.out 2>&1
./ResultsExtractorMain $nexpt input-list-master.txt best-fits-master.root > resultsextractor-master.out 2>&1
'


