
#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>

#include "TFile.h"
#include "TH2.h"
#include "TString.h"

#include "LauFitObject.hh"
#include "Lau1DHistPdf.hh"
#include "LauArgusPdf.hh"
#include "LauBkgndDPModel.hh"
#include "LauCleoCPCoeffSet.hh"
#include "LauCPFitModel.hh"
#include "LauDaughters.hh"
#include "LauEffModel.hh"
#include "LauGaussPdf.hh"
#include "LauIsobarDynamics.hh"
#include "LauLinearPdf.hh"
#include "LauResonanceMaker.hh"
#include "LauSumPdf.hh"
#include "LauVetoes.hh"

void usage( std::ostream& out, const TString& progName )
{
	out<<"Usage:\n";
	out<<progName<<" datafile gen [nExpt = 1] [firstExpt = 0]\n";
	out<<"or\n";
	out<<progName<<" datafile fit <iFit> <port> [hostname]  [nExpt = 1] [firstExpt = 0]"<<std::endl;
        out<<" Datafile format:\n";
        out<<" ---char-- -doubles- ---------------------------------\n";
        out<<" Resonance r delta b phi fix_r fix_delta fix_b fix_phi\n";
}

int main( int argc, char** argv )
{

	if ( argc < 2 ) {
		usage( std::cerr, argv[0] );
		return EXIT_FAILURE;
	}

        TString datafile =argv[1];
	TString command = argv[2];
	command.ToLower();
	Int_t iFit(0);
	UInt_t port(5000);
	TString hostname("localhost");
	Int_t nExpt(1);
	Int_t firstExpt(0);
	if ( command == "gen" ) {
		if ( argc > 3 ) {
			nExpt = atoi( argv[3] );
			if ( argc > 4 ) {
				firstExpt = atoi( argv[4] );
			}
		}
	} else if ( command == "fit" ) {
		if ( argc < 5 ) {
			usage( std::cerr, argv[0] );
			return EXIT_FAILURE;
		}
		iFit = atoi( argv[3] );
 		port = atoi( argv[4] );
		hostname = argv[5];
		if ( argc > 5 ) {
			nExpt = atoi( argv[6] );
			if ( argc > 6 ) {
				firstExpt = atoi( argv[7] );
			}
		}
	} else {
		usage( std::cerr, argv[0] );
		return EXIT_FAILURE;
	}

	// If you want to use square DP histograms for efficiency,
	// backgrounds or you just want the square DP co-ordinates
	// stored in the toy MC ntuple then set this to kTRUE
	Bool_t squareDP = kTRUE;

	// Set this to kFALSE if you want to remove the DP from the fit
	Bool_t doDP = kTRUE;

	// Set this to kTRUE if you want to fix the CPV parameters
	Bool_t fixCP = kFALSE;
	Bool_t doTwoStageFit = !fixCP;


	// Signal and continuum yields - from Phys.Rev.D78:012004,2008 - CAMBIAR
	Double_t nSigEvents = 5000;  //4585 ?
	Bool_t fixNSigEvents = kFALSE;
//	Double_t nBgEvents = 6830.0;
//	Bool_t fixNBgEvents = kFALSE;

	// Signal and continuum asymmetries
	// NB the signal asymmetry value here is only used if the DP is NOT
	// in the fit, otherwise the asymmetry is included within the
	// isobar model.
//	Double_t sigAsym = 0.028;
//	Bool_t fixSigAsym = kFALSE;
//	Double_t bgAsym = -0.028;
//	Bool_t fixBgAsym = kFALSE;

	// This defines the DP => decay is B+ -> K_S0 K+ K-
	// Particle 1 = K_S0
	// Particle 2 = K+
	// Particle 3 = K-
	// The DP is defined in terms of m13Sq and m23Sq
	// Define the DP (for both B+ and B- candidates)
	LauDaughters* negDaughters =  new LauDaughters("B0_bar",  "K+", "K-","K_S0", squareDP);
	LauDaughters* posDaughters =  new LauDaughters("B0",  "K-", "K+", "K_S0",squareDP);


	// Optionally apply some vetoes to the DP
	LauVetoes* vetoes = new LauVetoes();
	//Double_t DMin = 1.83;
	//Double_t DMax = 1.9;
	//Double_t JpsiMin = 3.05;
	//Double_t JpsiMax = 3.15;
	//Double_t psi2SMin = 3.676;
	//Double_t psi2SMax = 3.866;
	//vetoes->addMassVeto(2, DMin, DMax); // D0 veto, m13
	//vetoes->addMassVeto(2, JpsiMin, JpsiMax); // J/psi veto, m13
	//vetoes->addMassVeto(2, psi2SMin, psi2SMax); // psi(2S) veto, m13
	//vetoes->addMassVeto(1, DMin, DMax); // D0 veto, m23
	//vetoes->addMassVeto(1, JpsiMin, JpsiMax); // J/psi veto, m23
	//vetoes->addMassVeto(1, psi2SMin, psi2SMax); // psi(2S) veto, m23


	LauEffModel* negEffModel = new LauEffModel(negDaughters, vetoes);
	TFile *effHistFile = TFile::Open("LHCbEff/Spline_Efficiency_Map_Loose_Bd2KSKK_DD_2012b_KK.root", "read");
	TH2* effHist = dynamic_cast<TH2*>(effHistFile->Get("efficiency"));
	Bool_t useInterpolation = kTRUE;
	Bool_t fluctuateBins = kFALSE;
	Bool_t useUpperHalf = kTRUE;
	negEffModel->setEffHisto(effHist, useInterpolation, fluctuateBins, 0.0, 0.0, useUpperHalf, squareDP);

	LauEffModel* posEffModel = new LauEffModel(posDaughters, vetoes);
	posEffModel->setEffHisto(effHist, useInterpolation, fluctuateBins, 0.0, 0.0, useUpperHalf, squareDP);



	// Create the isobar model

	// Create the signal dynamics
	LauResonanceMaker& resMaker = LauResonanceMaker::get();
	resMaker.setDefaultBWRadius( LauBlattWeisskopfFactor::Parent,     5.0 );
	resMaker.setDefaultBWRadius( LauBlattWeisskopfFactor::Kstar,      4.0 );
	resMaker.setDefaultBWRadius( LauBlattWeisskopfFactor::Light,      4.0 );
	resMaker.setDefaultBWRadius( LauBlattWeisskopfFactor::Charmonium, 4.0 );
	resMaker.fixBWRadius( LauBlattWeisskopfFactor::Parent,  kTRUE );
	resMaker.fixBWRadius( LauBlattWeisskopfFactor::Light,  kTRUE );
	resMaker.fixBWRadius( LauBlattWeisskopfFactor::Charmonium,  kTRUE );
	resMaker.fixBWRadius( LauBlattWeisskopfFactor::Kstar,  kTRUE );

	LauIsobarDynamics* negSigModel = new LauIsobarDynamics(negDaughters, negEffModel);
	negSigModel->setIntFileName("integ_neg.dat");
	negSigModel->setASqMaxValue(0.03);

	LauAbsResonance* reson(0);
	reson = negSigModel->addResonance("phi(1020)",  3, LauAbsResonance::RelBW);  // resPairAmpInt = 1 => resonance mass is m23.
	reson = negSigModel->addResonance("f_0(980)",   3, LauAbsResonance::Flatte);
	reson->setResonanceParameter("g1",0.2);
	reson->setResonanceParameter("g2",1.0);
        reson = negSigModel->addResonance("f_0(1500)",   3, LauAbsResonance::RelBW);
        reson = negSigModel->addResonance("f_0(1710)",   3, LauAbsResonance::RelBW);
	reson = negSigModel->addResonance("f'_2(1525)",  3, LauAbsResonance::RelBW);  
	reson = negSigModel->addResonance("chi_c0",  3, LauAbsResonance::RelBW);

        reson = negSigModel->addResonance("PolNR_S0",  3, LauAbsResonance::PolNR); 
        reson = negSigModel->addResonance("PolNR_S1",  3, LauAbsResonance::PolNR);
        reson = negSigModel->addResonance("PolNR_S2",  3, LauAbsResonance::PolNR);
        reson = negSigModel->addResonance("PolNR_P0",  3, LauAbsResonance::PolNR);
//        reson = negSigModel->addResonance("PolNR_P1",  3, LauAbsResonance::PolNR);
        reson = negSigModel->addResonance("PolNR_P2",  3, LauAbsResonance::PolNR);

	LauIsobarDynamics* posSigModel = new LauIsobarDynamics(posDaughters, posEffModel);
	posSigModel->setIntFileName("integ_pos.dat");
	posSigModel->setASqMaxValue(0.03);

	reson = posSigModel->addResonance("phi(1020)",   3, LauAbsResonance::RelBW);  // resPairAmpInt = 1 => resonance mass is m23.
	reson = posSigModel->addResonance("f_0(980)",   3, LauAbsResonance::Flatte);
	reson->setResonanceParameter("g1",0.2);
	reson->setResonanceParameter("g2",1.0);
        reson = posSigModel->addResonance("f_0(1500)",   3, LauAbsResonance::RelBW);
        reson = posSigModel->addResonance("f_0(1710)",   3, LauAbsResonance::RelBW);
	reson = posSigModel->addResonance("f'_2(1525)",  3, LauAbsResonance::RelBW);  
	reson = posSigModel->addResonance("chi_c0",  3, LauAbsResonance::RelBW);

        reson = posSigModel->addResonance("PolNR_S0",  3, LauAbsResonance::PolNR); 
        reson = posSigModel->addResonance("PolNR_S1",  3, LauAbsResonance::PolNR);
        reson = posSigModel->addResonance("PolNR_S2",  3, LauAbsResonance::PolNR);
        reson = posSigModel->addResonance("PolNR_P0",  3, LauAbsResonance::PolNR);
//        reson = posSigModel->addResonance("PolNR_P1",  3, LauAbsResonance::PolNR);
        reson = posSigModel->addResonance("PolNR_P2",  3, LauAbsResonance::PolNR);

	// Reset the maximum signal DP ASq value
	// This will be automatically adjusted to avoid bias or extreme
	// inefficiency if you get the value wrong but best to set this by
	// hand once you've found the right value through some trial and
	// error.


	// Create the fit model, passing it the two signal dynamics models
	LauCPFitModel* fitModel = new LauCPFitModel(negSigModel,posSigModel,kTRUE);  //False= fit without tagging
	fitModel->useDP(doDP);

	// Create the complex coefficients for the isobar model

   std::vector<LauAbsCoeffSet*> coeffset;
        TString Resonance;
        int x,y;
  	float_t par[9][12];
 	ifstream in(datafile);

 	 for (y = 0; y < 11; y++) {
  	  for (x = 0; x < 9; x++) {
           if(x==0){in >> Resonance; std::cout << Resonance <<std::endl;}
           else{in >> par[x][y]; std::cout << par[x][y] <<std::endl;}
  	  }
coeffset.push_back( new LauCleoCPCoeffSet(Resonance,  par[1][y],  par[2][y] , par[3][y], par[4][y], par[5][y], par[6][y], par[7][y],  par[8][y], kTRUE, kTRUE ));
 	 }

 	 in.close();


   	//Set CP for each component
/*         for (unsigned int i=0; i!=coeffset.size();++i){
	 std::vector<LauParameter*> le=coeffset[i]->getParameters();
  	 double_t x=le[0]->value();double_t y=le[1]->value();double_t dx=le[2]->value();
         // if CP is set to 0%
         if(CPSetC==0){coeffset[i]->setParameterValue("DeltaY",0.0,kTRUE);coeffset[i]->setParameterValue("DeltaX",0.0,kTRUE);} else{
         //else
  	 double_t DeltaYSet= (2*y + (1-2*(y>=0))*TMath::Power(4*y*y-4*CPSetC*(CPSetC*(x*x+y*y+dx*dx) -2*dx*x) ,0.5))/( 2*CPSetC);
  	 coeffset[i]->setParameterValue("DeltaY",DeltaYSet,kTRUE);}
	}
*/
	for (std::vector<LauAbsCoeffSet*>::iterator iter=coeffset.begin(); iter!=coeffset.end(); ++iter) {
		fitModel->setAmpCoeffSet(*iter);
	}

   /*   // Adding constraints
         TString MagFormula("TMath::Sqrt([0]*[0]+[1]*[1])");   TString PhaseFormula("TMath::ATan([1]/[0])"); 

         for(int i=0; i<12; i++){
         TString AX("A"); AX+=i; AX+="_X";
         TString AY("A"); AY+=i; AY+="_Y";
         std::cout<<i<<" This is AX "<<AX<<std::endl;

         if(par[9][i]==1){ //if the mag is fixed
         std::vector<TString> ParamList;
         ParamList.push_back(AX);    ParamList.push_back(AY); 
         std::vector<LauParameter*> le=coeffset[i]->getParameters();
         double_t Refx=le[0]->value();double_t Refy=le[1]->value();
         double_t RefMag= TMath::Sqrt(Refx*Refx+Refy*Refy);  
         fitModel->addConstraint(MagFormula,ParamList, RefMag, 0.001);
         }
         if(par[10][i]==1){ //if the phase is fixed
         std::vector<TString> ParamList;
         ParamList.push_back(AX);    ParamList.push_back(AY); 
         std::vector<LauParameter*> le=coeffset[i]->getParameters();
         double_t Refx=le[0]->value();double_t Refy=le[1]->value();
         double_t RefPhase =TMath::ATan(Refy/Refx);
         fitModel->addConstraint(PhaseFormula,ParamList, RefPhase, 0.001);
         }
         }
*/
	// Set the signal yield and define whether it is fixed or floated

	LauParameter * signalEvents = new LauParameter("signalEvents",nSigEvents,-2.0*nSigEvents,2.0*nSigEvents,fixNSigEvents);
	fitModel->setNSigEvents(signalEvents);

	// Set the number of experiments to generate or fit and which
	// experiment to start with
	fitModel->setNExpts( nExpt, firstExpt );

	// Switch on/off calculation of asymmetric errors.
	fitModel->useAsymmFitErrors(kFALSE);

	// Randomise initial fit values for the signal mode
	fitModel->useRandomInitFitPars(kTRUE);

	// Switch on/off Poissonian smearing of total number of events
	fitModel->doPoissonSmearing(kTRUE);

	// Switch on/off Extended ML Fit option
	Bool_t emlFit = ( fitModel->nBkgndClasses() > 0 );
	fitModel->doEMLFit(emlFit);

        //fitModel->writeLatexTable(kTRUE);

        // Set the names of the files to read/write, name coming from dataset name
        TString fileHead = datafile.operator()(0,datafile.Last('.')); // operator = substring //  with "" doesn't work
	TString dataFile("Gen_"); dataFile +=fileHead; dataFile +=".root";
        //TString dataFile("CP_Eff.root");
	TString treeName("genResults");
	TString rootFileName("");
	TString tableFileName("");
	TString fitToyFileName("CP_0_fitToyMC_");
	TString splotFileName("splot_");
	if (command == "fit") {
		rootFileName = "Fits/fit_"; rootFileName +=fileHead; rootFileName +="-";   rootFileName += iFit;
		rootFileName += "_expt_"; rootFileName += firstExpt;
		rootFileName += "-"; rootFileName += (firstExpt+nExpt-1);
		rootFileName += ".root";
		tableFileName = "CP_fitResults_"; tableFileName += iFit;
		fitToyFileName += iFit;
		fitToyFileName += ".root";
		splotFileName += iFit;
		splotFileName += ".root";
	} else {
		rootFileName = "dummy.root";
		tableFileName = "genResults";
	}

	// Generate toy from the fitted parameters
	//fitModel->compareFitData(1000, fitToyFileName);

	// Write out per-event likelihoods and sWeights
	//fitModel->writeSPlotData(splotFileName, "splot", kFALSE);

	// Execute the generation/fit
	if ( command == "fit" ) {
		fitModel->runSlave( dataFile, treeName, rootFileName, tableFileName, hostname, port );
	} else {
		fitModel->run( command, dataFile, treeName, rootFileName, tableFileName );
	}

	return EXIT_SUCCESS;
}
