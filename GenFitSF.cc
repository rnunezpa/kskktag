
#include <cstdlib>
#include <iostream>
#include <vector>
#include <fstream>

#include "TFile.h"
#include "TH2.h"
#include "TString.h"
#include "TTree.h"

#include "LauSimpleFitModel.hh"
#include "LauBkgndDPModel.hh"
#include "LauDaughters.hh"
#include "LauEffModel.hh"
#include "LauIsobarDynamics.hh"
#include "LauMagPhaseCoeffSet.hh"
#include "LauResonanceMaker.hh"
#include "LauVetoes.hh"

void usage( std::ostream& out, const TString& progName )
{
	out<<"Usage:\n";
	out<<progName<<" datafile gen [nExpt = 1] [firstExpt = 0]\n";
	out<<"or\n";
	out<<progName<<" datafile fit <iFit> <port> [hostname] [nExpt = 1] [firstExpt = 0]"<<std::endl;
}

int main( int argc, char** argv )
{

	if ( argc < 2 ) {
		usage( std::cerr, argv[0] );
		return EXIT_FAILURE;
	}
       TString datafile =argv[1];
	TString command = argv[2];
	command.ToLower();
	Int_t iFit(0);
	UInt_t port(5000);
	TString hostname("localhost");
	Int_t nExpt(1);
	Int_t firstExpt(0);
	if ( command == "gen" ) {
		if ( argc > 3 ) {
			nExpt = atoi( argv[3] );
			if ( argc > 4 ) {
				firstExpt = atoi( argv[4] );
			}
		}
	} else if ( command == "fit" ) {
		if ( argc < 5 ) {
			usage( std::cerr, argv[0] );
			return EXIT_FAILURE;
		}
		iFit = atoi( argv[3] );
 		port = atoi( argv[4] );
		hostname = argv[5];
		if ( argc > 5 ) {
			nExpt = atoi( argv[6] );
			if ( argc > 6 ) {
				firstExpt = atoi( argv[7] );
			}
		}
	} else {
		usage( std::cerr, argv[0] );
		return EXIT_FAILURE;
	}


	// If you want to use square DP histograms for efficiency,
	// backgrounds or you just want the square DP co-ordinates
	// stored in the toy MC ntuple then set this to kTRUE
	Bool_t squareDP = kTRUE;

	// This defines the DP => decay is B+ -> K_S0 K+ K-
	// Particle 1 = K+
	// Particle 2 = K-
	// Particle 3 = K_S0
	// The DP is defined in terms of m13Sq and m23Sq
	LauDaughters* daughters = new LauDaughters("B0","K+", "K-","K_S0", squareDP);

	// Optionally apply some vetoes to the DP
	LauVetoes* vetoes = new LauVetoes();
	//Double_t DMin = 1.83;
	//Double_t DMax = 1.9;
	//Double_t JpsiMin = 3.05;
	//Double_t JpsiMax = 3.15;
	//Double_t psi2SMin = 3.676;
	//Double_t psi2SMax = 3.866;
	//vetoes->addMassVeto(2, DMin, DMax); // D0 veto, m13
	//vetoes->addMassVeto(2, JpsiMin, JpsiMax); // J/psi veto, m13
	//vetoes->addMassVeto(2, psi2SMin, psi2SMax); // psi(2S) veto, m13
	//vetoes->addMassVeto(1, DMin, DMax); // D0 veto, m23
	//vetoes->addMassVeto(1, JpsiMin, JpsiMax); // J/psi veto, m23
	//vetoes->addMassVeto(1, psi2SMin, psi2SMax); // psi(2S) veto, m23

	// Define the efficiency model (defaults to unity everywhere)
	// Can optionally provide a histogram to model variation over DP
	// (example syntax given in commented-out section)
	LauEffModel* effModel = new LauEffModel(daughters, vetoes);
	TFile *effHistFile = TFile::Open("LHCbEff/Spline_Efficiency_Map_Loose_Bd2KSKK_DD_2012b_KK.root", "read");
	TH2* effHist = dynamic_cast<TH2*>(effHistFile->Get("efficiency"));
	Bool_t useInterpolation = kTRUE;
	Bool_t fluctuateBins = kFALSE;
	Bool_t useUpperHalf = kTRUE;
	effModel->setEffHisto(effHist, useInterpolation, fluctuateBins, 0.0, 0.0, useUpperHalf, squareDP);

	// Create the isobar model

	// Set the values of the Blatt-Weisskopf barrier radii and whether they are fixed or floating
	// Create the signal dynamics
	LauResonanceMaker& resMaker = LauResonanceMaker::get();
	resMaker.setDefaultBWRadius( LauBlattWeisskopfFactor::Parent,     5.0 );
	resMaker.setDefaultBWRadius( LauBlattWeisskopfFactor::Kstar,      4.0 );
	resMaker.setDefaultBWRadius( LauBlattWeisskopfFactor::Light,      4.0 );
	resMaker.setDefaultBWRadius( LauBlattWeisskopfFactor::Charmonium, 4.0 );
	resMaker.fixBWRadius( LauBlattWeisskopfFactor::Parent,  kTRUE );
	resMaker.fixBWRadius( LauBlattWeisskopfFactor::Light,  kTRUE );
	resMaker.fixBWRadius( LauBlattWeisskopfFactor::Charmonium,  kTRUE );
	resMaker.fixBWRadius( LauBlattWeisskopfFactor::Kstar,  kTRUE );


	LauIsobarDynamics* sigModel = new LauIsobarDynamics(daughters, effModel);
	LauAbsResonance* reson(0);
	reson = sigModel->addResonance("phi(1020)",   3, LauAbsResonance::RelBW);  // resPairAmpInt = 1 => resonance mass is m23.
	reson = sigModel->addResonance("f_0(980)",   3, LauAbsResonance::Flatte);
	reson->setResonanceParameter("g1",0.2);
	reson->setResonanceParameter("g2",1.0);
        reson = sigModel->addResonance("f_0(1500)",   3, LauAbsResonance::RelBW);
        reson = sigModel->addResonance("f_0(1710)",   3, LauAbsResonance::RelBW);
	reson = sigModel->addResonance("f'_2(1525)",  3, LauAbsResonance::RelBW);  
	reson = sigModel->addResonance("chi_c0",  3, LauAbsResonance::RelBW);

        reson = sigModel->addResonance("PolNR_S0",  3, LauAbsResonance::PolNR); 
        reson = sigModel->addResonance("PolNR_S1",  3, LauAbsResonance::PolNR);
        reson = sigModel->addResonance("PolNR_S2",  3, LauAbsResonance::PolNR);
        reson = sigModel->addResonance("PolNR_P0",  3, LauAbsResonance::PolNR);
 //       reson = sigModel->addResonance("PolNR_P1",  3, LauAbsResonance::PolNR); 	
        reson = sigModel->addResonance("PolNR_P2",  3, LauAbsResonance::PolNR);


	// Reset the maximum signal DP ASq value
	// This will be automatically adjusted to avoid bias or extreme
	// inefficiency if you get the value wrong but best to set this by
	// hand once you've found the right value through some trial and
	// error.
	sigModel->setASqMaxValue(0.03);  

	// Create the fit model
	LauSimpleFitModel* fitModel = new LauSimpleFitModel(sigModel);

	// Create the complex coefficients for the isobar model

   std::vector<LauAbsCoeffSet*> coeffset;
        TString Resonance;
        int x,y;
  	float_t par[9][12];
 	 ifstream in(datafile);

 	 for (y = 0; y < 11; y++) {
  	  for (x = 0; x < 5; x++) {
           if(x==0){in >> Resonance; std::cout << Resonance <<std::endl;}
           else{in >> par[x][y]; std::cout << par[x][y] <<std::endl;}
  	  }
coeffset.push_back( new LauMagPhaseCoeffSet(Resonance,  par[1][y],  par[2][y] , par[3][y], par[4][y] ) );
                coeffset[y]->finaliseValues();
 	 }

 	 in.close();

	for (std::vector<LauAbsCoeffSet*>::iterator iter=coeffset.begin(); iter!=coeffset.end(); ++iter) {
		fitModel->setAmpCoeffSet(*iter);
	}

	// Set the signal yield and define whether it is fixed or floated
	const Double_t nSig = 1500.0;
	LauParameter * nSigEvents = new LauParameter("nSigEvents",nSig,-2.0*nSig,2.0*nSig,kFALSE);
	fitModel->setNSigEvents(nSigEvents);

	// Set the number of experiments to generate or fit and which
	// experiment to start with
	fitModel->setNExpts( nExpt, firstExpt );

	// Optionally load in continuum background DP model histogram
	// (example syntax given in commented-out section)
	std::vector<TString> bkgndNames(1);
	bkgndNames[0] = "qqbar";
	fitModel->setBkgndClassNames( bkgndNames );
	const Double_t nBkg = 0.0;
	LauParameter* nBkgndEvents = new LauParameter("qqbar",nBkg,-2.0*nBkg,2.0*nBkg,kFALSE);
	fitModel->setNBkgndEvents( nBkgndEvents );
	//TString qqFileName("histoFiles/offResDP.root");
	//TFile* qqFile = TFile::Open(qqFileName.Data(), "read");
	//TH2* qqDP = dynamic_cast<TH2*>(qqFile->Get("AllmTheta")); // m', theta'
	LauBkgndDPModel* qqbarModel = new LauBkgndDPModel(daughters, vetoes);
	//qqbarModel->setBkgndHisto(qqDP, useInterpolation, fluctuateBins, useUpperHalf, squareDP);
	fitModel->setBkgndDPModel( "qqbar", qqbarModel );

	// Switch on/off calculation of asymmetric errors.
	fitModel->useAsymmFitErrors(kFALSE);

	// Randomise initial fit values for the signal mode
	fitModel->useRandomInitFitPars(kTRUE);

	// Switch on/off Poissonian smearing of total number of events
	fitModel->doPoissonSmearing(kTRUE);

	// Switch on/off Extended ML Fit option
	Bool_t emlFit = ( fitModel->nBkgndClasses() > 0 );
	fitModel->doEMLFit(emlFit);

        // Set the names of the files to read/write, name coming from dataset name
        TString fileHead = datafile.operator()(0,datafile.Last('.')); // operator = substring //  with "" doesn't work
	TString dataFile("Gen_"); dataFile +=fileHead; dataFile +=".root";
        //TString dataFile("CP_Eff.root");
	TString treeName("genResults");
	TString rootFileName("");
	TString tableFileName("");
	TString fitToyFileName("fitToyMC_");
	TString splotFileName("splot_");
	if (command == "fit") {
		rootFileName = "Fits/fit_"; rootFileName +=fileHead; rootFileName +="-";   rootFileName += iFit;
		rootFileName += "_expt_"; rootFileName += firstExpt;
		rootFileName += "-"; rootFileName += (firstExpt+nExpt-1);
		rootFileName += ".root";
		tableFileName = "fitResults_"; tableFileName += iFit;
		fitToyFileName += iFit;
		fitToyFileName += ".root";
		splotFileName += iFit;
		splotFileName += ".root";
	} else {
		rootFileName = "dummy.root";
		tableFileName = "genResults";
	}

	// Generate toy from the fitted parameters
	//fitModel->compareFitData(100, fitToyFileName);

	// Write out per-event likelihoods and sWeights
	//fitModel->writeSPlotData(splotFileName, "splot", kFALSE);

	// Execute the generation/fit
	if ( command == "fit" ) {
		fitModel->runSlave( dataFile, treeName, rootFileName, tableFileName, hostname, port );
	} else {
		fitModel->run( command, dataFile, treeName, rootFileName, tableFileName );
	}

	return EXIT_SUCCESS;
}
